// gobutil provides useful tools for use when gob encoding/decoding.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package gobutil

import (
	"bytes"
	"encoding/gob"
	"reflect"
	"sync"
)

// Common error
const illegalUninitialisedEncoder = "illegal use of an uninitialised encoder"

// Encoder provides a gob encoder backed by a bytes buffer.
type Encoder struct {
	buf bytes.Buffer
	enc *gob.Encoder
}

// pool is a pool of Encoders ready for use.
var pool *sync.Pool

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the pool.
func init() {
	pool = &sync.Pool{
		New: func() interface{} {
			return &Encoder{}
		},
	}
}

/////////////////////////////////////////////////////////////////////////
// Encoder functions
/////////////////////////////////////////////////////////////////////////

// Encode transmits the data item represented by the empty interface value, guaranteeing that all necessary type information has been transmitted first.
func (enc *Encoder) Encode(e interface{}) error {
	if enc.enc == nil {
		panic(illegalUninitialisedEncoder)
	}
	return enc.enc.Encode(e)
}

// EncodeValue transmits the data item represented by the reflection value, guaranteeing that all necessary type information has been transmitted first.
func (enc *Encoder) EncodeValue(value reflect.Value) error {
	if enc.enc == nil {
		panic(illegalUninitialisedEncoder)
	}
	return enc.enc.EncodeValue(value)
}

// Bytes returns the slice of bytes encoded to-date.
func (enc *Encoder) Bytes() []byte {
	if enc.enc == nil {
		panic(illegalUninitialisedEncoder)
	}
	b := enc.buf.Bytes()
	d := make([]byte, len(b))
	copy(d, b)
	return d
}

// Reset resets the encoder.
func (enc *Encoder) Reset() {
	enc.buf.Reset()
	enc.enc = gob.NewEncoder(&enc.buf)
}

// Len returns the number of bytes used by the encoder's underlying buffer.
func (enc *Encoder) Len() int {
	if enc.enc == nil {
		panic(illegalUninitialisedEncoder)
	}
	return enc.buf.Len()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewEncoder returns a new encoder.
func NewEncoder() *Encoder {
	enc := pool.Get().(*Encoder)
	enc.enc = gob.NewEncoder(&enc.buf)
	return enc
}

// ReuseEncoder returns the given encoder to a pool ready for reuse by subsequent calls to NewEncoder.
func ReuseEncoder(enc *Encoder) {
	if enc != nil {
		enc.enc = nil
		enc.buf.Reset()
		pool.Put(enc)
	}
}

// NewDecoder returns a new decoder for the given slice of bytes.
func NewDecoder(buf []byte) *gob.Decoder {
	return gob.NewDecoder(bytes.NewBuffer(buf))
}
